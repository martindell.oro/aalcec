<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Doctrine\Common\Persistence\ObjectManager;

class PacientesType extends AbstractType
{
    private $manager;

    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('protocolo', TextType::class, array('attr'=>array('placeholder'=>'#####/## ó ######/##')))
        ->add('apellido')
        ->add('nombre')
        ->add('sexo', ChoiceType::class, array(
                      "required"=>true,
                      'choices'  => array(
                          ''=>null,
                          'Femenino' => 'Femenino',
                          'Masculino' => 'Masculino',
                      )))
        ->add('provinciaId', HiddenType::class)
        ->add('departamentoId', HiddenType::class)
        ->add('departamentoNombre', HiddenType::class)
        ->add('localidadId', HiddenType::class)
        ->add('localidadNombre', EntityType::class, array("attr"=> array("class"=>"form-control"),'class'=>'AppBundle\Entity\Localidades','label'=>'Localidad','placeholder' => "",
            'query_builder' => function ($er) {
                                return $er->createQueryBuilder('u')
                                    ->where("u.codpcia='30'")
                                    ->andWhere("u.show=true")
                                    ->orderBy('u.departamen', 'DESC');
                            },
            'group_by'=> function ($dato) {
                return $dato->getDepartamen();
            }))
        ->add('fechaMuestra', DateType::class, array("required"=>false,'widget' => 'single_text',))
        ->add('dni', TextType::class, array('label'=>'DNI'))
        ->add('edad')
        ->add('material')
        ->add('diagnostico')
        ->add('tipoCancer')
        ->add('observacion', TextareaType::class, array("required"=>false));


        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $form=$event->getForm();
            $data=$event->getData();
            // $localidad=;
            $localidades = $this->manager->getRepository('AppBundle:Localidades')->findOneByGid($data['localidadNombre']);
            $data['provinciaId']=$localidades->getCodpcia();
            $data['departamentoId']=$localidades->getCoddpto();
            $data['departamentoNombre']=$localidades->getDepartamen();
            $event->setData($data);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Pacientes'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_pacientes';
    }
}
