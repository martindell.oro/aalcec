<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Pacientes
 *
 * @ORM\Table(name="pacientes", indexes={@ORM\Index(name="IDX_971B78517A94BA1A", columns={"diagnostico_id"}), @ORM\Index(name="IDX_971B7851E308AC6F", columns={"material_id"})})
 * @ORM\Entity
 * @UniqueEntity("protocolo")
 * @ORM\HasLifecycleCallbacks
 */
class Pacientes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="pacientes_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="protocolo", type="string", nullable=true)
     * @Assert\Regex(
     *     pattern     = "/\b(([0-9]{5})|([0-9]{6}))\b\/[0-9]{2}/",
     *     message = "El formato debe ser #####/## ó ######/##"
     * )
     * @Assert\Length(min = 8, max=9, exactMessage="El campo debe tener {{ limit }} digitos, #####/##")
     * @Assert\NotBlank(message="El campo no puede estar vacío")
     */
    private $protocolo;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", nullable=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido", type="string", nullable=true)
     */
    private $apellido;

    /**
     * @var integer
     *
     * @ORM\Column(name="provincia_id", type="integer", nullable=true)
     */
    private $provinciaId;

    /**
     * @var string
     *
     * @ORM\Column(name="departamento_nombre", type="string", nullable=true)
     */
    private $departamentoNombre;

    /**
     * @var integer
     *
     * @ORM\Column(name="departamento_id", type="integer", nullable=true)
     */
    private $departamentoId;

    /**
     * @var string
     *
     * @ORM\Column(name="localidad_nombre", type="string", nullable=true)
     */
    private $localidadNombre;

    /**
     * @var integer
     *
     * @ORM\Column(name="localidad_id", type="integer", nullable=true)
     */
    private $localidadId;

    /**
     * @var string
     *
     * @ORM\Column(name="dni", type="string", nullable=true)
     */
    private $dni;

    /**
     * @var integer
     *
     * @ORM\Column(name="edad", type="integer", nullable=true)
     */
    private $edad;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_muestra", type="date", nullable=true)
     */
    private $fechaMuestra;

    /**
     * @var string
     *
     * @ORM\Column(name="created_by", type="string", nullable=true)
     */
    private $createdBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="date", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="date", nullable=true)
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="updated_by", type="string", nullable=true)
     */
    private $updatedBy;

    /**
     * @var string
     *
     * @ORM\Column(name="sexo", type="string", nullable=true)
     */
    private $sexo;

    /**
     * @var string
     *
     * @ORM\Column(name="observacion", type="string", nullable=true)
     */
    private $observacion;

    /**
     * @var \Diagnosticos
     *
     * @ORM\ManyToOne(targetEntity="Diagnosticos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="diagnostico_id", referencedColumnName="id")
     * })
     */
    private $diagnostico;

    /**
     * @var \MaterialesRemitidos
     *
     * @ORM\ManyToOne(targetEntity="MaterialesRemitidos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="material_id", referencedColumnName="id")
     * })
     */
    private $material;

    /**
     * @var \TiposCancer
     *
     * @ORM\ManyToOne(targetEntity="TiposCancer", fetch="LAZY")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipo_cancer", referencedColumnName="id")
     * })
     */
    private $tipoCancer;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Gets triggered only on insert

     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update

     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime("now");
    }

    /**
     * Set protocolo
     *
     * @param string $protocolo
     *
     * @return Pacientes
     */
    public function setProtocolo($protocolo)
    {
        $this->protocolo = $protocolo;

        return $this;
    }

    /**
     * Get protocolo
     *
     * @return string
     */
    public function getProtocolo()
    {
        return $this->protocolo;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Pacientes
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     *
     * @return Pacientes
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * Get apellido
     *
     * @return string
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * Set departamentoNombre
     *
     * @param string $departamentoNombre
     *
     * @return Pacientes
     */
    public function setDepartamentoNombre($departamentoNombre)
    {
        $this->departamentoNombre = $departamentoNombre;

        return $this;
    }

    /**
     * Get departamentoNombre
     *
     * @return string
     */
    public function getDepartamentoNombre()
    {
        return $this->departamentoNombre;
    }

    /**
     * Set provinciaId
     *
     * @param integer $provinciaId
     *
     * @return Pacientes
     */
    public function setProvinciaId($provinciaId)
    {
        $this->provinciaId = $provinciaId;

        return $this;
    }

    /**
     * Get provinciaID
     *
     * @return integer
     */
    public function getProvinciaId()
    {
        return $this->provinciaId;
    }
    /**
     * Set departamentoId
     *
     * @param integer $departamentoId
     *
     * @return Pacientes
     */
    public function setDepartamentoId($departamentoId)
    {
        $this->departamentoId = $departamentoId;

        return $this;
    }

    /**
     * Get departamentoId
     *
     * @return integer
     */
    public function getDepartamentoId()
    {
        return $this->departamentoId;
    }

    /**
     * Set localidadNombre
     *
     * @param string $localidadNombre
     *
     * @return Pacientes
     */
    public function setLocalidadNombre($localidadNombre)
    {
        $this->localidadNombre = $localidadNombre;

        return $this;
    }

    /**
     * Get localidadNombre
     *
     * @return string
     */
    public function getLocalidadNombre()
    {
        return $this->localidadNombre;
    }

    /**
     * Set localidadId
     *
     * @param integer $localidadId
     *
     * @return Pacientes
     */
    public function setLocalidadId($localidadId)
    {
        $this->localidadId = $localidadId;

        return $this;
    }

    /**
     * Get localidadId
     *
     * @return integer
     */
    public function getLocalidadId()
    {
        return $this->localidadId;
    }

    /**
     * Set dni
     *
     * @param string $dni
     *
     * @return Pacientes
     */
    public function setDni($dni)
    {
        $this->dni = $dni;

        return $this;
    }

    /**
     * Get dni
     *
     * @return string
     */
    public function getDni()
    {
        return $this->dni;
    }

    /**
     * Set edad
     *
     * @param integer $edad
     *
     * @return Pacientes
     */
    public function setEdad($edad)
    {
        $this->edad = $edad;

        return $this;
    }

    /**
     * Get edad
     *
     * @return integer
     */
    public function getEdad()
    {
        return $this->edad;
    }

    /**
     * Set fechaMuestra
     *
     * @param \DateTime $fechaMuestra
     *
     * @return Pacientes
     */
    public function setFechaMuestra($fechaMuestra)
    {
        $this->fechaMuestra = $fechaMuestra;

        return $this;
    }

    /**
     * Get fechaMuestra
     *
     * @return \DateTime
     */
    public function getFechaMuestra()
    {
        return $this->fechaMuestra;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     *
     * @return Pacientes
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Pacientes
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Pacientes
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedBy
     *
     * @param string $updatedBy
     *
     * @return Pacientes
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return string
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set sexo
     *
     * @param string $sexo
     *
     * @return Pacientes
     */
    public function setSexo($sexo)
    {
        $this->sexo = $sexo;

        return $this;
    }

    /**
     * Get sexo
     *
     * @return string
     */
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * Set observacion
     *
     * @param string $observacion
     *
     * @return Pacientes
     */
    public function setObservacion($observacion)
    {
        $this->observacion = $observacion;

        return $this;
    }

    /**
     * Get observacion
     *
     * @return string
     */
    public function getObservacion()
    {
        return $this->observacion;
    }

    /**
     * Set diagnostico
     *
     * @param \AppBundle\Entity\Diagnosticos $diagnostico
     *
     * @return Pacientes
     */
    public function setDiagnostico(\AppBundle\Entity\Diagnosticos $diagnostico = null)
    {
        $this->diagnostico = $diagnostico;

        return $this;
    }

    /**
     * Get diagnostico
     *
     * @return \AppBundle\Entity\Diagnosticos
     */
    public function getDiagnostico()
    {
        return $this->diagnostico;
    }

    /**
     * Set material
     *
     * @param \AppBundle\Entity\MaterialesRemitidos $material
     *
     * @return Pacientes
     */
    public function setMaterial(\AppBundle\Entity\MaterialesRemitidos $material = null)
    {
        $this->material = $material;

        return $this;
    }

    /**
     * Get material
     *
     * @return \AppBundle\Entity\MaterialesRemitidos
     */
    public function getMaterial()
    {
        return $this->material;
    }

    /**
     * Set material
     *
     * @param \AppBundle\Entity\MaterialesRemitidos $material
     *
     * @return Pacientes
     */
    public function setTipoCancer(\AppBundle\Entity\TiposCancer $tipocancer = null)
    {
        $this->tipoCancer = $tipocancer;

        return $this;
    }

    /**
     * Get material
     *
     * @return \AppBundle\Entity\MaterialesRemitidos
     */
    public function getTipoCancer()
    {
        return $this->tipoCancer;
    }
}
