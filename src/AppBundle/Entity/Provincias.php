<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Provincias
 *
 * @ORM\Table(name="provincias")
 * @ORM\Entity
 */
class Provincias
{
    /**
     * @var integer
     *
     * @ORM\Column(name="gid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="provincias_gid_seq", allocationSize=1, initialValue=1)
     */
    private $gid;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=254, nullable=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="desde", type="string", length=8, nullable=true)
     */
    private $desde;

    /**
     * @var string
     *
     * @ORM\Column(name="hasta", type="string", length=8, nullable=true)
     */
    private $hasta;

    /**
     * @var integer
     *
     * @ORM\Column(name="estilo_id", type="integer", nullable=true)
     */
    private $estiloId;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=254, nullable=true)
     */
    private $link;

    /**
     * @var integer
     *
     * @ORM\Column(name="fuente_id", type="integer", nullable=true)
     */
    private $fuenteId;

    /**
     * @var integer
     *
     * @ORM\Column(name="toponimo_i", type="integer", nullable=true)
     */
    private $toponimoI;

    /**
     * @var string
     *
     * @ORM\Column(name="geojson", type="string", nullable=true)
     */
    private $geojson;


}

