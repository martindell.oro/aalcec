<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Localidades
 *
 * @ORM\Table(name="localidades")
 * @ORM\Entity
 */
class Localidades
{
    /**
     * @var integer
     *
     * @ORM\Column(name="gid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="localidades_gid_seq", allocationSize=1, initialValue=1)
     */
    private $gid;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=254, nullable=true)
     */
    private $link;

    /**
     * @var string
     *
     * @ORM\Column(name="codpcia", type="string", length=254, nullable=true)
     */
    private $codpcia;

    /**
     * @var string
     *
     * @ORM\Column(name="coddpto", type="string", length=254, nullable=true)
     */
    private $coddpto;

    /**
     * @var string
     *
     * @ORM\Column(name="codloc", type="string", length=254, nullable=true)
     */
    private $codloc;

    /**
     * @var string
     *
     * @ORM\Column(name="codgoblo", type="string", length=254, nullable=true)
     */
    private $codgoblo;

    /**
     * @var string
     *
     * @ORM\Column(name="provincia", type="string", length=254, nullable=true)
     */
    private $provincia;

    /**
     * @var string
     *
     * @ORM\Column(name="departamen", type="string", length=254, nullable=true)
     */
    private $departamen;

    /**
     * @var string
     *
     * @ORM\Column(name="localidad", type="string", length=254, nullable=true)
     */
    private $localidad;

    /**
     * @var string
     *
     * @ORM\Column(name="func_loc", type="string", length=254, nullable=true)
     */
    private $funcLoc;

    /**
     * @var string
     *
     * @ORM\Column(name="tiploc", type="string", length=254, nullable=true)
     */
    private $tiploc;

    /**
     * @var string
     *
     * @ORM\Column(name="tip2loc", type="string", length=254, nullable=true)
     */
    private $tip2loc;

    /**
     * @var string
     *
     * @ORM\Column(name="latitud", type="string", length=254, nullable=true)
     */
    private $latitud;

    /**
     * @var string
     *
     * @ORM\Column(name="longitud", type="string", length=254, nullable=true)
     */
    private $longitud;

    /**
     * @var string
     *
     * @ORM\Column(name="xgk", type="decimal", precision=10, scale=0, nullable=true)
     */
    private $xgk;

    /**
     * @var string
     *
     * @ORM\Column(name="ygk", type="decimal", precision=10, scale=0, nullable=true)
     */
    private $ygk;

    /**
     * @var string
     *
     * @ORM\Column(name="geojson", type="string", nullable=true)
     */
    private $geojson;

    /**
     * @var boolean
     *
     * @ORM\Column(name="show", type="boolean", nullable=true)
     */
    private $show;

    /**
     * Get departamen
     *
     * @return string
     */
    public function getDepartamen()
    {
        return $this->departamen;
    }
    public function getCodpcia()
    {
        return $this->codpcia;
    }
    public function getCoddpto()
    {
        return $this->coddpto;
    }

    public function __toString(){
      return $this->localidad;
    }
}
