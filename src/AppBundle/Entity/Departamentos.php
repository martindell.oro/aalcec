<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Departamentos
 *
 * @ORM\Table(name="departamentos")
 * @ORM\Entity
 */
class Departamentos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="gid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="departamentos_gid_seq", allocationSize=1, initialValue=1)
     */
    private $gid;

    /**
     * @var string
     *
     * @ORM\Column(name="shape_leng", type="decimal", precision=10, scale=0, nullable=true)
     */
    private $shapeLeng;

    /**
     * @var string
     *
     * @ORM\Column(name="shape_area", type="decimal", precision=10, scale=0, nullable=true)
     */
    private $shapeArea;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=254, nullable=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="provincia_", type="string", length=254, nullable=true)
     */
    private $provincia;

    /**
     * @var string
     *
     * @ORM\Column(name="cod_depto_", type="string", length=254, nullable=true)
     */
    private $codDepto;

    /**
     * @var string
     *
     * @ORM\Column(name="cabecera", type="string", length=254, nullable=true)
     */
    private $cabecera;

    /**
     * @var string
     *
     * @ORM\Column(name="fuente_", type="string", length=254, nullable=true)
     */
    private $fuente;

    /**
     * @var integer
     *
     * @ORM\Column(name="cod_prov", type="integer", nullable=true)
     */
    private $codProv;

    /**
     * @var string
     *
     * @ORM\Column(name="geojson", type="string", nullable=true)
     */
    private $geojson;


}

