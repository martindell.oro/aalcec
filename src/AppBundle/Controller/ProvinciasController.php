<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query ;

/**
 * Plantaciones controller.
 *
 * @Route("admin/provincias")
 */
class ProvinciasController extends Controller
{
    /**
     * @Route("/provincias", name="")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
        ]);
    }
    /* Obtengo todos los paises*/
    /**
     * Finds and displays a Paises entity.
     *
     * @Route("/paises.json", name="json_paises")
     * @Method("GET")
     */
    public function jsonPaisessAction(Request $request)
    {
        $query = $this->getDoctrine()
        ->getRepository('AppBundle:Paises')
        ->createQueryBuilder('c')
        ->select('c.nombre', 'c.iso3')
        ->getQuery();
        $result = $query->getResult(Query::HYDRATE_ARRAY);
        $group = array();

        foreach ($result as $key => $value) {
            $group[$key]['label'] = $value['nombre'];
            $group[$key]['value'] = $value['iso3'];
        }
        $response = new Response();
        $response->setContent(json_encode($group));
        $response->headers->set('Content-Type', 'application/json');


        return $response;
    }
    /* Obtengo Todas las provincias*/
    /**
     * Finds and displays a Plantaciones entity.
     *
     * @Route("/provincias.json", name="json_provincias")
     * @Method("GET")
     */
    public function jsonProvinciasAction(Request $request)
    {
        // $em = $this->getDoctrine()->getManager();
        // $departamentos = $em->getRepository('AppBundle:Provincias')->findAll();
        $query = $this->getDoctrine()
        ->getRepository('AppBundle:Provincias')
        ->createQueryBuilder('c')
        ->select('c.nombre', 'c.gid')
        ->getQuery();
        $result = $query->getResult(Query::HYDRATE_ARRAY);
        $group = array();

        foreach ($result as $key => $value) {
            $group[$key]['label'] = $value['nombre'];
            $group[$key]['value'] = $value['gid'];
        }
        $response = new Response();
        $response->setContent(json_encode($group));
        $response->headers->set('Content-Type', 'application/json');


        return $response;
    }
    /* Obtengo Departamentos*/
    /**
     * Finds and displays a Plantaciones entity.
     *
     * @Route("/{id}/departamentos.json", name="json_departamentos")
     * @Method("GET")
     */
    public function jsonDepartamentosAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        // $departamentos = $em->getRepository('AppBundle:Departamentos')->findByProvincia($id);
        $query = $em->createQueryBuilder()
                  ->select('d')
                  ->from('AppBundle:Departamentos', 'd')
                  ->select('d.nombre', 'd.codDepto')
                  ->where('d.codProv = :id')
                  ->setParameter('id', $id)
                  ->getQuery();
        $result = $query->getResult(Query::HYDRATE_ARRAY);
        $group = array();

        foreach ($result as $key => $value) {
            $group[$key]['label'] = $value['nombre'];
            $group[$key]['value'] = $value['codDepto'];
        }
        $response = new Response();
        $response->setContent(json_encode($group));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /* Obtengo Localidades*/
    /**
     * Finds and displays a Localidades entity.
     *
     * @Route("/{id}/localidades.json", name="json_localidades")
     * @Method("GET")
     */
    public function jsonLocalidadesAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare('SELECT l.localidad, l.codpcia || l.coddpto || l.codloc as codLoc
                                            from departamentos d
                                            left join arg_localidades l on l.codpcia || l.coddpto = d.cod_depto_
                                            WHERE l.codpcia || l.coddpto =:id
                                            group by l.localidad, l.codpcia || l.coddpto || l.codloc');
        $statement->bindValue('id', $id);
        $statement->execute();
        $results = $statement->fetchAll();
        $group = array();

        foreach ($results as $key => $value) {
            $group[$key]['label'] = $value['localidad'];
            $group[$key]['value'] = $value['codloc'];
        }
        $response = new Response();
        $response->setContent(json_encode($group));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /* Obtengo Departamentos*/
    /**
     * Finds and displays a Localidades entity.
     *
     * @Route("/dptolocalidades.json", name="json_dptolocalidades")
     * @Method("GET")
     */
    public function jsonDptoLocalidadesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        // $departamentos = $em->getRepository('AppBundle:Departamentos')->findAll();

        $statement = $connection->prepare('SELECT d.cod_depto_, d.nombre, d.provincia_
                                            FROM Departamentos d
                                            WHERE d.cod_prov = 12');
        $statement->execute();
        $departamentos = $statement->fetchAll();

        $query = $connection->prepare("SELECT l.codloc, l.departamen, l.localidad, l.codpcia , l.coddpto
                                        FROM localidades l
                                        WHERE l.codpcia = '30'");
        $query->execute();
        $localidades = $query->fetchAll();

        $group = array();
        foreach ($departamentos as $key => $value) {
          $subgroup = array();
          $group[$key]['id'] = (integer)$value['cod_depto_'];
          $group[$key]['text'] = $value['nombre'];
          foreach ($localidades as $k => $valor) {
            if($value['cod_depto_'] == ($valor['codpcia'] . $valor['coddpto'])) {
              $subgroup[$k]['id']= (integer)$valor['codloc'];
              $subgroup[$k]['text']= $valor['localidad'];
            }
            $group[$key]['children']=$subgroup;
          }
        }
        // foreach ($results as $key => $value) {
        //     $group[$key]['label'] = $value['localidad'];
        //     $group[$key]['value'] = $value['codloc'];
        // }
        $response = new Response();
        $response->setContent(json_encode($group));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
