<?php

namespace AppBundle\Controller;

use AppBundle\Entity\MaterialesRemitidos;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\FormInterface;

/**
 * Materialesremitido controller.
 *
 * @Route("admin/materialesremitidos")
 */
class MaterialesRemitidosController extends Controller
{
    /**
     * Lists all materialesRemitido entities.
     *
     * @Route("/", name="materialesremitidos_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $dql   = "SELECT a
          FROM AppBundle:MaterialesRemitidos a";
        $query = $em->createQuery($dql);
        $paginator = $this->get('knp_paginator');
        $materialesRemitidos = $paginator->paginate(
              $query,
              $request->query->getInt('page', 1),
              15,
              array('orderBy' => 'a.createdAt', 'defaultSortDirection' => 'ASC')
          );

        return $this->render('materialesremitidos/index.html.twig', array(
            'materialesRemitidos' => $materialesRemitidos,
        ));
    }

    /**
     * Creates a new materialesRemitido entity.
     *
     * @Route("/new", name="materialesremitidos_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $materialesRemitido = new Materialesremitidos();
        $form = $this->createForm('AppBundle\Form\MaterialesRemitidosType', $materialesRemitido);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($materialesRemitido);
            $em->flush();

            return $this->redirectToRoute('materialesremitidos_show', array('id' => $materialesRemitido->getId()));
        }

        return $this->render('materialesremitidos/new.html.twig', array(
            'materialesRemitido' => $materialesRemitido,
            'form' => $form->createView(),
        ));
    }

    private function getErrorsFromForm(FormInterface $form)
    {
        $errors = array();
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }
        return $errors;
    }
    /**
       * Creates a new cliente entity.
       *
       * @Route("/new_material", name="material_create")
       * @Method({"GET", "POST"})
       */
      public function createAction(Request $request)
      {
          if (!$request->isXmlHttpRequest()) {
              return new JsonResponse(array('message' => 'You cant access, gato!'), 400);
          }
          $materialesRemitido = new Materialesremitidos();
          $form = $this->createForm('AppBundle\Form\MaterialesRemitidosType', $materialesRemitido);
          $form->handleRequest($request);

          if ($request->getMethod() == 'POST') {
              if ($form->isSubmitted() && $form->isValid()) {
                  $em = $this->getDoctrine()->getManager();
                  $em->persist($materialesRemitido);
                  $em->flush();
                  return new JsonResponse(array('message' => 'Success!', 'number'=>'1', 'id'=>$materialesRemitido->getId(),'descripcion'=>$materialesRemitido->getNombre()), 200);
              } else {
                $errors = $this->getErrorsFromForm($form);
                $data = [
                  'type' => 'validation_error',
                  'title' => 'There was a validation error',
                  'errors' => $errors ];
                return new JsonResponse(array('errors'=>$errors),400);
              }
          }
          if ($request->getMethod() == 'GET') {
              return $this->render('materialesremitidos/materialModal.html.twig', array(
              'materialesRemitido' => $materialesRemitido,
              'form' => $form->createView(),
          ));
          }
      }
    /**
     * Finds and displays a materialesRemitido entity.
     *
     * @Route("/{id}", name="materialesremitidos_show")
     * @Method("GET")
     */
    public function showAction(MaterialesRemitidos $materialesRemitido)
    {
        $deleteForm = $this->createDeleteForm($materialesRemitido);

        return $this->render('materialesremitidos/show.html.twig', array(
            'materialesRemitido' => $materialesRemitido,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing materialesRemitido entity.
     *
     * @Route("/{id}/edit", name="materialesremitidos_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, MaterialesRemitidos $materialesRemitido)
    {
        $deleteForm = $this->createDeleteForm($materialesRemitido);
        $editForm = $this->createForm('AppBundle\Form\MaterialesRemitidosType', $materialesRemitido);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('materialesremitidos_index');
        }

        return $this->render('materialesremitidos/edit.html.twig', array(
            'materialesRemitido' => $materialesRemitido,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a materialesRemitido entity.
     *
     * @Route("/{id}", name="materialesremitidos_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, MaterialesRemitidos $materialesRemitido)
    {
        $form = $this->createDeleteForm($materialesRemitido);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($materialesRemitido);
            $em->flush();
        }

        return $this->redirectToRoute('materialesremitidos_index');
    }

    /**
     * Creates a form to delete a materialesRemitido entity.
     *
     * @param MaterialesRemitidos $materialesRemitido The materialesRemitido entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(MaterialesRemitidos $materialesRemitido)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('materialesremitidos_delete', array('id' => $materialesRemitido->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
