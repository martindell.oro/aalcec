<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Reservas;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Report controller.
 *
 * @Route("report")
 */
class ReportController extends Controller
{
    /**
    * Lista todos los pacientes con cancer agrupado por año
    *
    * @Route("/pacientes_anio", name="report_anio", options={"expose"=true})
    * @Method("GET")
    */
    public function jsonDayction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare('select extract(year from fecha_muestra) as year, count(id) as value
                                          from pacientes
                                          where fecha_muestra is not null
                                          group by extract(year from fecha_muestra)
                                          order by year');
        $statement->execute();
        $results = $statement->fetchAll();

        $response = new Response();
        $response->setContent(json_encode($results)); //results
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }


    /**
    * Lista todos los pacientes con cancer agrupado por año
    *
    * @Route("/pacientes_anio_depto", name="pacientes_anio_depto", options={"expose"=true})
    * @Method("GET")
    */
    public function jsonAnioDeptoction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare('select departamento_nombre as departamento, count(id) as value
                                              from pacientes
                                              group by departamento_nombre
                                              order by departamento_nombre
                                        ');
        $statement->execute();
        $results = $statement->fetchAll();

        $response = new Response();
        $response->setContent(json_encode($results)); //results
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }


    /**
    * Lista todos los pacientes PACIENTES SEGUN SEXO
    *
    * @Route("/pacientes_sexo", name="pacientes_sexo", options={"expose"=true})
    * @Method("GET")
    */
    public function jsonSexoAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare('select sexo as label, count(id) as value
                                            from pacientes
                                            group by sexo
                                            order by sexo
                                        ');
        $statement->execute();
        $results = $statement->fetchAll();

        $response = new Response();
        $response->setContent(json_encode($results)); //results
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
    * Lista todos los PACIENTES POR SEXO POR AÑO
    *
    * @Route("/pacientes_sexo_anio", name="pacientes_sexo_anio", options={"expose"=true})
    * @Method("GET")
    */
    public function jsonSexoAnioction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare('select sexo as label, extract(year from fecha_muestra) as year, count(id) as value
        from pacientes
        where fecha_muestra is not null
        group by sexo, extract(year from fecha_muestra)
        order by  sexo,extract(year from fecha_muestra) ');
        $statement->execute();
        $results = $statement->fetchAll();

        $response = new Response();
        $response->setContent(json_encode($results)); //results
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
    * Lista todos los pacientes con cancer agrupado por depto
    *
    * @Route("/pacientes_geo", name="pacientes_geo", options={"expose"=true})
    * @Method("GET")
    */
    public function jsonGeoDptoction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare('select departamento_nombre as departamento, count(id) as value, d.geojson
                                          from pacientes p
                                          join departamentos d on p.departamento_nombre ilike d.nombre
                                          where substring(d.cod_depto_ from 1 for 2)::integer = p.provincia_id
                                          group by departamento_nombre, d.geojson
                                          order by departamento_nombre
                                        ');
        $statement->execute();
        $results = $statement->fetchAll();
        $geodata[] = array();
        foreach ($results as $key => $value) {
          $geodata[$key]['type']='Feature';
          $geodata[$key]['properties']=array('departamento'=>$value['departamento'],'value'=>$value['value']);
          $geodata[$key]['geometry']=json_decode($value['geojson'],true);
        }
        $response = new Response();
        $response->setContent(json_encode($geodata));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
    * Lista todos los pacientes con cancer agrupado por ciudad
    *
    * @Route("/pacientes_geo_city", name="pacientes_geo_city", options={"expose"=true})
    * @Method("GET")
    */
    public function jsonGeoCityAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare('select localidad_nombre as localidad, count(id) as value, l.geojson
                                            from pacientes p
                                            join localidades l on l.gid = p.localidad_id
                                            group by localidad_nombre, l.geojson
                                            order by localidad_nombre
                                        ');
        $statement->execute();
        $results = $statement->fetchAll();
        $geodata[] = array();
        foreach ($results as $key => $value) {
          $geodata[$key]['type']='Feature';
          $geodata[$key]['properties']=array('localidad'=>$value['localidad'],'value'=>$value['value']);
          $geodata[$key]['geometry']=json_decode($value['geojson'],true);
        }
        $response = new Response();
        $response->setContent(json_encode($geodata));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
    * Lista todos los pacientes con cancer agrupado por ciudad
    *
    * @Route("/count", name="pacientes_count", options={"expose"=true})
    * @Method("GET")
    */
    public function jsonCountAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare('select count(*)
                                            from pacientes p
                                        ');
        $statement->execute();
        $results = $statement->fetchAll();

        $response = new Response();
        $response->setContent(json_encode($results[0]));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

}
