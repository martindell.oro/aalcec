<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Pacientes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Paciente controller.
 *
 * @Route("admin/pacientes")
 */
class PacientesController extends Controller
{
    /**
     * Lists all paciente entities.
     *
     * @Route("/", name="pacientes_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $dql   = "SELECT a
          FROM AppBundle:Pacientes a
          ORDER BY a.protocolo asc";
        $query = $em->createQuery($dql);
        $paginator = $this->get('knp_paginator');
        $pacientes = $paginator->paginate(
              $query,
              $request->query->getInt('page', 1),
              15,
              array('orderBy' => 'a.createdAt', 'defaultSortDirection' => 'ASC')
          );
        return $this->render('pacientes/index.html.twig', array(
            'pacientes' => $pacientes,
        ));
    }

    /**
     * Creates a new paciente entity.
     *
     * @Route("/new", name="pacientes_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $paciente = new Pacientes();
        $form = $this->createForm('AppBundle\Form\PacientesType', $paciente);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($paciente);
            $em->flush();

            return $this->redirectToRoute('pacientes_show', array('id' => $paciente->getId()));
        }

        return $this->render('pacientes/new.html.twig', array(
            'paciente' => $paciente,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a paciente entity.
     *
     * @Route("/{id}", name="pacientes_show")
     * @Method("GET")
     */
    public function showAction(Pacientes $paciente)
    {
        $deleteForm = $this->createDeleteForm($paciente);

        return $this->render('pacientes/show.html.twig', array(
            'paciente' => $paciente,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing paciente entity.
     *
     * @Route("/{id}/edit", name="pacientes_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Pacientes $paciente)
    {
        $deleteForm = $this->createDeleteForm($paciente);
        $editForm = $this->createForm('AppBundle\Form\PacientesType', $paciente);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('pacientes_index');
        }

        return $this->render('pacientes/edit.html.twig', array(
            'paciente' => $paciente,
            'localidadId'=> ($paciente->getLocalidadId()) ? $paciente->getLocalidadId(): 'no',
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a paciente entity.
     *
     * @Route("/{id}", name="pacientes_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Pacientes $paciente)
    {
        $form = $this->createDeleteForm($paciente);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($paciente);
            $em->flush();
        }

        return $this->redirectToRoute('pacientes_index');
    }

    /**
     * Creates a form to delete a paciente entity.
     *
     * @param Pacientes $paciente The paciente entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Pacientes $paciente)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('pacientes_delete', array('id' => $paciente->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
