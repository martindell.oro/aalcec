<?php

namespace AppBundle\Controller;

use AppBundle\Entity\TiposCancer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Tiposcancer controller.
 *
 * @Route("admin/tiposcancer")
 */
class TiposCancerController extends Controller
{
    /**
     * Lists all tiposCancer entities.
     *
     * @Route("/", name="tiposcancer_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        // $em = $this->getDoctrine()->getManager();
        // $tiposCancers = $em->getRepository('AppBundle:TiposCancer')->findAll();
        $dql   = "SELECT a
          FROM AppBundle:TiposCancer a
          ORDER BY a.nombre asc";
        $query = $em->createQuery($dql);
        $paginator = $this->get('knp_paginator');
        $tiposCancers = $paginator->paginate(
              $query,
              $request->query->getInt('page', 1),
              15,
              array('orderBy' => 'a.createdAt', 'defaultSortDirection' => 'ASC')
          );
        return $this->render('tiposcancer/index.html.twig', array(
            'tiposCancers' => $tiposCancers,
        ));
    }

    /**
     * Creates a new tiposCancer entity.
     *
     * @Route("/new", name="tiposcancer_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $tiposCancer = new Tiposcancer();
        $form = $this->createForm('AppBundle\Form\TiposCancerType', $tiposCancer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tiposCancer);
            $em->flush();

            return $this->redirectToRoute('tiposcancer_show', array('id' => $tiposCancer->getId()));
        }

        return $this->render('tiposcancer/new.html.twig', array(
            'tiposCancer' => $tiposCancer,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a tiposCancer entity.
     *
     * @Route("/{id}", name="tiposcancer_show")
     * @Method("GET")
     */
    public function showAction(TiposCancer $tiposCancer)
    {
        $deleteForm = $this->createDeleteForm($tiposCancer);

        return $this->render('tiposcancer/show.html.twig', array(
            'tiposCancer' => $tiposCancer,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing tiposCancer entity.
     *
     * @Route("/{id}/edit", name="tiposcancer_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, TiposCancer $tiposCancer)
    {
        $deleteForm = $this->createDeleteForm($tiposCancer);
        $editForm = $this->createForm('AppBundle\Form\TiposCancerType', $tiposCancer);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tiposcancer_edit', array('id' => $tiposCancer->getId()));
        }

        return $this->render('tiposcancer/edit.html.twig', array(
            'tiposCancer' => $tiposCancer,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a tiposCancer entity.
     *
     * @Route("/{id}", name="tiposcancer_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, TiposCancer $tiposCancer)
    {
        $form = $this->createDeleteForm($tiposCancer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tiposCancer);
            $em->flush();
        }

        return $this->redirectToRoute('tiposcancer_index');
    }

    /**
     * Creates a form to delete a tiposCancer entity.
     *
     * @param TiposCancer $tiposCancer The tiposCancer entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TiposCancer $tiposCancer)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tiposcancer_delete', array('id' => $tiposCancer->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
