<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * Accionescooperacion controller.
 *
 * @Route("/admin/usuarios")
 */
class UsuariosController extends Controller
{
    /**
     * Lists all accionesCooperacion entities.
     *
     * @Route("/", name="usuarios_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $usuarios = $em->getRepository('AppBundle:User')->findAll();

        return $this->render('usuarios/index.html.twig', array(
          'usuarios' => $usuarios,
      ));
    }

    /**
     * Finds and displays a contraparte entity.
     *
     * @Route("/{id}", name="usuarios_show")
     * @Method("GET")
     */
    public function showAction(User $usuario)
    {
        $deleteForm = $this->createDeleteForm($usuario);

        return $this->render('usuarios/show.html.twig', array(
            'usuario' => $usuario,
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Edit user
     *
     * @Route("/edit/{id}", name="usuarios_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, User $user)
    {
        $formFactory = $this->get('fos_user.registration.form.factory');
        $editForm = $formFactory->createForm();
        $editForm->setData($user);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            try {
                $userManager = $this->container->get('fos_user.user_manager');
                $user->setPlainPassword($user->getPlainPassword());
                $userManager->updatePassword($user);
                $this->getDoctrine()->getManager()->flush();
                $this->get('session')->getFlashBag()->add('notice', array('type' => 'success', 'title' => '', 'message' => 'Edición satisfactoria.'));
                return $this->redirectToRoute('usuarios_index');
            } catch (\Doctrine\ORM\ORMException $e) {
                $this->get('session')->getFlashBag()->add('error', 'Ocurrió un error al editar ');
                $this->get('logger')->error($e->getMessage());
                return $this->redirectToRoute('usuarios_index');
            }
        }

        return $this->render('@FOSUser/Profile/edit.html.twig', array(
          'form' => $editForm->createView(),
          'usuario' => $user,
      ));
    }

    /**
     * Deletes a areasgestora entity.
     *
     * @Route("/{id}", name="usuarios_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, User $user)
    {
        $form = $this->createDeleteForm($user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->remove($user);
                $em->flush();
                $this->get('session')->getFlashBag()->add('notice', array('type' => 'success', 'title' => '', 'message' => 'Se ha eliminado el usuario.'));
                return $this->redirectToRoute('usuarios_index');
            } catch (\Doctrine\ORM\ORMException $e) {
                $this->get('session')->getFlashBag()->add('error', 'Ocurrió un error al eliminar ');
                $this->get('logger')->error($e->getMessage());
                return $this->redirectToRoute('usuarios_index');
            }
        }
    }

    /**
     * Creates a form to delete a areasgestora entity.
     *
     * @param Areasgestoras $areasgestora The areasgestora entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(User $user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('usuarios_delete', array('id' => $user->getId())))
            ->setMethod('DELETE')
            ->add('submit', SubmitType::class, array('label' => 'Eliminar Usuario',
                                            'attr' => array('class' => 'btn btn-danger btn-lg btn btn-primary pull-right', "onclick"=>"return confirm('¿Está seguro que desea eliminar?')", "type"=> "button")))
            ->getForm()
        ;
    }
}
