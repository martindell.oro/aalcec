<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Diagnosticos;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\FormInterface;

/**
 * Diagnostico controller.
 *
 * @Route("admin/diagnosticos")
 */
class DiagnosticosController extends Controller
{
    /**
     * Lists all diagnostico entities.
     *
     * @Route("/", name="diagnosticos_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $dql   = "SELECT a
          FROM AppBundle:Diagnosticos a";
        $query = $em->createQuery($dql);
        $paginator = $this->get('knp_paginator');
        $diagnosticos = $paginator->paginate(
              $query,
              $request->query->getInt('page', 1),
              15,
              array('orderBy' => 'a.createdAt', 'defaultSortDirection' => 'ASC')
          );

        return $this->render('diagnosticos/index.html.twig', array(
            'diagnosticos' => $diagnosticos,
        ));
    }

    /**
     * Creates a new diagnostico entity.
     *
     * @Route("/new", name="diagnosticos_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $diagnostico = new Diagnosticos();
        $form = $this->createForm('AppBundle\Form\DiagnosticosType', $diagnostico);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($diagnostico);
            $em->flush();

            return $this->redirectToRoute('diagnosticos_show', array('id' => $diagnostico->getId()));
        }

        return $this->render('diagnosticos/new.html.twig', array(
            'diagnostico' => $diagnostico,
            'form' => $form->createView(),
        ));
    }

    private function getErrorsFromForm(FormInterface $form)
    {
        $errors = array();
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }
        return $errors;
    }
    /**
       * Creates a new cliente entity.
       *
       * @Route("/new_diagnostico", name="diagnostico_create")
       * @Method({"GET", "POST"})
       */
      public function createAction(Request $request)
      {
          if (!$request->isXmlHttpRequest()) {
              return new JsonResponse(array('message' => 'You cant access, gato!'), 400);
          }
          $diagnostico = new Diagnosticos();
          $form = $this->createForm('AppBundle\Form\DiagnosticosType', $diagnostico);
          $form->handleRequest($request);

          if ($request->getMethod() == 'POST') {
              if ($form->isSubmitted() && $form->isValid()) {
                  $em = $this->getDoctrine()->getManager();
                  $em->persist($diagnostico);
                  $em->flush();
                  return new JsonResponse(array('message' => 'Success!', 'number'=>'1', 'id'=>$diagnostico->getId(),'descripcion'=>$diagnostico->getGrupo().'-'.$diagnostico->getSubgrupo()), 200);
              } else {
                $errors = $this->getErrorsFromForm($form);
                $data = [
                  'type' => 'validation_error',
                  'title' => 'There was a validation error',
                  'errors' => $errors ];
                return new JsonResponse(array('errors'=>$errors),400);
              }
          }
          if ($request->getMethod() == 'GET') {
              return $this->render('diagnosticos/diagnosticoModal.html.twig', array(
              'diagnostico' => $diagnostico,
              'form' => $form->createView(),
          ));
          }
      }

    /**
     * Finds and displays a diagnostico entity.
     *
     * @Route("/{id}", name="diagnosticos_show")
     * @Method("GET")
     */
    public function showAction(Diagnosticos $diagnostico)
    {
        $deleteForm = $this->createDeleteForm($diagnostico);

        return $this->render('diagnosticos/show.html.twig', array(
            'diagnostico' => $diagnostico,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing diagnostico entity.
     *
     * @Route("/{id}/edit", name="diagnosticos_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Diagnosticos $diagnostico)
    {
        $deleteForm = $this->createDeleteForm($diagnostico);
        $editForm = $this->createForm('AppBundle\Form\DiagnosticosType', $diagnostico);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('diagnosticos_index');
        }

        return $this->render('diagnosticos/edit.html.twig', array(
            'diagnostico' => $diagnostico,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a diagnostico entity.
     *
     * @Route("/{id}", name="diagnosticos_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Diagnosticos $diagnostico)
    {
        $form = $this->createDeleteForm($diagnostico);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($diagnostico);
            $em->flush();
        }

        return $this->redirectToRoute('diagnosticos_index');
    }

    /**
     * Creates a form to delete a diagnostico entity.
     *
     * @param Diagnosticos $diagnostico The diagnostico entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Diagnosticos $diagnostico)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('diagnosticos_delete', array('id' => $diagnostico->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
