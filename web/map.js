$(document).ready(function() {
    var mymap = L.map('map').setView([-32.164, -58.818], 7);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
            '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="http://mapbox.com">Mapbox</a>',
        id: 'mapbox.streets'
    }).addTo(mymap);

    var district_boundary = new L.geoJson();
    district_boundary.addTo(mymap);

    $.ajax({
        dataType: "json",
        url: "report/pacientes_geo",
        success: function(data) {
          district_boundary.addData(data);
          L.geoJSON(data, {
          style: function(feature) {
              var value = feature.properties.value;
              if (value >= 1 && value <= 200){
                return {color: "#ffff00"};
              }
              if (value >= 201 && value <= 500) {
                return {color: "#ffa500"};
              }
              if (value >= 501 && value <= 1000) {
                return {color: "#ff0000"};
              }
          },
          onEachFeature: function (feature, layer) {
            layer.bindPopup('<h5>'+'Departamento: '+'<b>'+feature.properties.departamento+'</b>'+'</h5><h5>Cantidad de casos: '+'<b>'+feature.properties.value+'</b>'+'</h5>');
          }
          }).addTo(mymap);
        }
    });


    var point = new L.geoJson();
    point.addTo(mymap);

    // $.ajax({
    //     dataType: "json",
    //     url: "report/pacientes_geo_city.json",
    //     success: function(data) {
    //       L.geoJSON(data, {
    //           pointToLayer: function (feature, latlng) {
    //               return L.circleMarker(latlng, {
    //                   radius: 15,
    //                   fillColor: "#ff7800",
    //                   color: "#000",
    //                   weight: 1,
    //                   opacity: 1,
    //                   fillOpacity: 0.8
    //               });
    //           }
    //       }).addTo(mymap);
    //     }
    // });

    var popup = L.popup();

    function onMapClick(e) {
        popup
            .setLatLng(e.latlng)
            .setContent("You clicked the map at " + e.latlng.toString())
            .openOn(mymap);
    }

    mymap.on('click', onMapClick);

});
