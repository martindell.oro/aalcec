$(document).ready(function() {
  $('#materialTuvie').on('click', function() {
    $('.form-group').removeClass('has-error');
    $('.form-group').tooltip('destroy');
    $('[data-toggle="tooltip"]').tooltip('destroy');
    var data = $("#form_material").serialize();
    var l = Ladda.create(this);
    l.start();
    $.ajax({
      type: 'POST',
      data: data,
      url: "/admin/materialesremitidos/new_material",
      success: function(data) {
        if (data['number'] == '1') {
          l.stop();
          $.sweetModal({
            content: 'Ha sido registrado con éxito',
            icon: $.sweetModal.ICON_SUCCESS,
            buttons: [
              {
                label: 'OK',
                classes: 'greenB'
              }
            ]
          });
          $('.modal').on('hidden.bs.modal', function() {
            $(this).find('form')[0].reset();
          });
          $("#newMaterial").modal('toggle');
          $('#appbundle_pacientes_material').append($('<option>', {
            value: data.id,
            text: data.descripcion
        }));
        $("appbundle_pacientes_material select").val();
        $("select option[value="+data.id+"]").attr("selected","selected");
        }
      },
      error: function(jqXHR) {
        l.stop();
        $.each(jqXHR.responseJSON.errors, function(k, v) {
          errorsText = "";
          if ((v) && (v.length > 0)) {
            $.each(v, function(n, errorText) {
              errorsText += errorText;
            });
            $('#appbundle_clientes_' + k).tooltip({'title': errorsText});
            $('#appbundle_clientes_' + k).closest('div[class="form-group"]').addClass('has-error');
          } else {
            $('#appbundle_clientes_' + k).closest('div[class="form-group has-error"]').removeClass('has-error');
            $('#appbundle_clientes_' + k).tooltip('destroy');
          }

        });
      }
    });
  });


  $('#diagnosticoTuvie').on('click', function() {
    $("#appbundle_diagnosticos_grupo").trigger("click")
    $('.form-group').removeClass('has-error');
    $('.form-group').tooltip('destroy');
    $('[data-toggle="tooltip"]').tooltip('destroy');
    var data = $("#form_diagnostico").serialize();
    var l = Ladda.create(this);
    l.start();
    $.ajax({
      type: 'POST',
      data: data,
      url: "/admin/diagnosticos/new_diagnostico",
      success: function(data) {
        if (data['number'] == '1') {
          l.stop();
          $.sweetModal({
            content: 'Ha sido registrado con éxito',
            icon: $.sweetModal.ICON_SUCCESS,
            buttons: [
              {
                label: 'OK',
                classes: 'greenB'
              }
            ]
          });
          $('.modal').on('hidden.bs.modal', function() {
            $(this).find('form')[0].reset();
          });
          $("#newDiagnostico").modal('toggle');
          $('#appbundle_pacientes_diagnostico').append($('<option>', {
            value: data.id,
            text: data.descripcion
        }));
        $("appbundle_pacientes_diagnostico select").val();
        $("select option[value="+data.id+"]").attr("selected","selected");
        }
      },
      error: function(jqXHR) {
        l.stop();
        $.each(jqXHR.responseJSON.errors, function(k, v) {
          errorsText = "";
          if ((v) && (v.length > 0)) {
            $.each(v, function(n, errorText) {
              errorsText += errorText;
            });
            $('#appbundle_clientes_' + k).tooltip({'title': errorsText});
            $('#appbundle_clientes_' + k).closest('div[class="form-group"]').addClass('has-error');
          } else {
            $('#appbundle_clientes_' + k).closest('div[class="form-group has-error"]').removeClass('has-error');
            $('#appbundle_clientes_' + k).tooltip('destroy');
          }

        });
      }
    });
  });

});
