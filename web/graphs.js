"use strict";
$(document).ready(function() {
    $.getJSON("report/pacientes_anio", function (json) { // callback function which gets called when your request completes.
        Morris.Bar({
            element: 'pacientes_anio',
            data: json,
            xkey: 'year',
            ykeys: ['value'],
            labels: ['Personas'],
            hideHover: "auto",
            behaveLikeLine: !0,
            resize: !0
        });
    });

    $.getJSON("report/pacientes_anio_depto", function (json) { // callback function which gets called when your request completes.
        Morris.Bar({
            element: 'pacientes_anio_depto',
            data: json,
            xkey: 'departamento',
            ykeys: ['value'],
            labels: ['Personas'],
            hideHover: "auto",
            behaveLikeLine: !0,
            resize: !0
        });
    });

    $.getJSON("report/pacientes_sexo", function (json) { // callback function which gets called when your request completes.
        Morris.Donut({
            element: 'pacientes_sexo',
            data: json,
            labels: ['Personas'],
            hideHover: "auto",
            behaveLikeLine: !0,
            resize: !0
        });
    });

    $.ajax({
        dataType: "json",
        url: "report/count",
        success: function(data) {
          $('#numpacientes').text(data['count']);
          /*numpacientes*/
        }
    });
});
